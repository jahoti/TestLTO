/*
	A script to load media on theonion.com, released under the Apache License 2.0
	Copyright © 2020 jahoti (jahoti@ctrl-c.club)

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

document.querySelectorAll('img[data-expanded-srcset]').forEach(
	function (node) {
		var data = node.getAttribute('data-expanded-srcset').split(' '), alt = document.createElement('a');
		alt.href = node.src = data[data.length-2];
		alt.textContent = 'Link to the image (use if loading fails)';
		node.insertAdjacentElement('afterend', alt);
	}
);
document.querySelectorAll('iframe[id^="megaphone-"]').forEach(
	function (node) {
		var data = node.id.split('-');
		var audio = document.createElement('audio'), alt = document.createElement('a');
		audio.setAttribute('controls', '');
		alt.href = audio.src = 'http://traffic.megaphone.fm/'+data[data.length-1]+'.mp3';
		node.parentNode.replaceChild(audio, node);
		alt.textContent = 'Link to the audio (use if loading fails)';
		audio.insertAdjacentElement('afterend', alt);
	}
);
